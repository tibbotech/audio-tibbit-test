const spawn = require('child_process').spawn;
var gpio = require("@tibbo-tps/gpio");

function nodeMicStream () {
    //var args = '-c 2 -r 44100 -f S16_LE --buffer-size=16384'.split(' ');
    var args = '-c 1 -r 16000 -f S16_LE --buffer-size=16384 example.raw'.split(' ');
    return spawn('arecord', args);
}

function nodePlayStream () {
    //var args = '-c 2 -r 44100 -f S16_LE --buffer-size=16384'.split(' ');
    var args = '-c 1 -r 16000 -f S16_LE --buffer-size=16384 example.raw'.split(' ');
    return spawn('aplay', args);
}

var play, record;
var button = gpio.init("S3A");
var wasButtonPressed = false;
button.setDirection("input");

setInterval(function(){
    // If button is just released...
    if(button.getValue() === 1 && wasButtonPressed === true){
        if(record){
            record.kill('SIGTERM');
        }
        play = nodePlayStream ();

        wasButtonPressed = false;

        console.log("released");
    }else if(button.getValue() === 0 && wasButtonPressed === false){
        // If button is pressed
        if(play){
            play.kill('SIGTERM');
        }
        record = nodeMicStream ();

        wasButtonPressed = true;

        console.log("pressed");
    }
},100);
